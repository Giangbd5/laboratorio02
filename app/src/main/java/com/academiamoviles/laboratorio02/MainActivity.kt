package com.academiamoviles.laboratorio02

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerificar.setOnClickListener() {
            val anio: Int=txtAño.text.toString().toInt()

            if(anio > 2010){
                txtLinea1.text="Año fuera del Rango"
            }else if(anio >= 1994){
                txtLinea1.text="Generación Z"
            }else if(anio >= 1981){
                txtLinea1.text="Generación Y"
            }else if(anio >= 1969){
                txtLinea1.text="Generación X"
            }else if(anio >= 1949){
                txtLinea1.text="Baby Boom"
            }else if(anio >= 1930){
                txtLinea1.text="Silent Generation"
            }else{
                txtLinea1.text="Año fuera del rango"
            }
        }
    }
}